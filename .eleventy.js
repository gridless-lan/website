const siteConfig = require('./config.json');

module.exports = function (eleventyConfig) {
  // Compile Main SCSS
  const scss = require(`${siteConfig.paths.src}/js/sass.js`);
  scss(`${siteConfig.paths.src}/scss/vendor/bootstrap/bootstrap.scss`, `${siteConfig.paths.src}/css/vendor/bootstrap/bootstrap.css`);
  scss(`${siteConfig.paths.src}/scss/styles.scss`, `${siteConfig.paths.src}/css/styles.css`);
  scss(`${siteConfig.paths.src}/scss/themes/default.scss`,`${siteConfig.paths.src}/css/themes/default.css`);

  /**
   * Add passthrough copies
   * See for more info: https://www.11ty.io/docs/copy/
   */
  eleventyConfig.addPassthroughCopy(`${siteConfig.paths.src}/css`);
  eleventyConfig.addPassthroughCopy(`${siteConfig.paths.src}/js`);

  return {
    dir: {
      input: siteConfig.paths.src,
      layouts: `_includes/layouts`,
      output: siteConfig.paths.output,
    }
  }
}