/**
 * THIS SCRIPT IS NOT INTENDED TO BE SERVED TO THE END-USER!
 */

const sass = require('sass');
const fs = require('fs-extra');
const path = require('path');

/**
 * Compile SCSS
 * @param scssPath
 * @param cssPath
 */
module.exports = (scssPath, cssPath) => {
  // Check if cssPath exists
  if(!fs.existsSync(path.dirname(cssPath))) {
    const result = sass.renderSync({file: scssPath});
      // Create the cssPath directory recursively
      fs.mkdir(path.dirname(cssPath), {recursive: true})

      // Write resulting css to cssPath file
      .then(() => fs.writeFile(cssPath, result.css.toString()))

      // Catch any error we may have encountered
      .catch(error => console.error(error))
  }
  
  // Watch for any changes to scssPath directory
  fs.watch(path.dirname(scssPath), () => {
    console.log(`Watching ${path.dirname(scssPath)}...`);

    // Compile the CSS
    const watchResult = sass.renderSync({file: scssPath});

    // Write resulting css to cssPath file
    fs.writeFile(cssPath, watchResult.css.toString())

    // Catch any error
    .catch(error => console.error(error))      
  });
}