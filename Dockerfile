# Build the thing first
#FROM node:10-alpine
#WORKDIR /app
#RUN npm install -g @11ty/eleventy
#COPY . /app
#RUN yarn install
#RUN yarn eleventy:build

FROM alpine:3.12
LABEL maintainer="Aroop Roelofs <me@finlaydag33k.nl>"

# Install dependencies
RUN apk update && \
    apk add --no-cache \
    lighttpd \
    curl

# Add Config files
COPY config/lighttpd/*.conf /etc/lighttpd/

# Healthcheck lighttpd
HEALTHCHECK --interval=1m --timeout=1s \
    CMD curl -f http://localhost/ || exit 1

# Expose ports
EXPOSE 80

# Make config path and webroot volumes
VOLUME /etc/lighttpd/

# Set out entrypoint
ENTRYPOINT ["/usr/sbin/lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]

# Add the required files
RUN mkdir -p /var/www/htdocs
#COPY dist /var/www/htdocs
RUN echo '<h1>Work in progress... please come back later!</h1>' >> /var/www/htdocs/index.html
COPY config/gridless-lan/* /var/www/htdocs/.well-known/gridless-lan/ 